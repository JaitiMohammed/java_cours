package Exercice5;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		
		File F1 = new File("FileStatistics.txt");
		File F2 = new File("FileStatistics02.txt");
		
		
		FileStatistics f1 = new FileStatistics(F1.toString());
		FileStatistics f2 = new FileStatistics(F2.toString());
		
		
		System.out.println("---Le 1er fichier  :" + F1.getName() + " -----");
		
		System.out.println("Le nombre de caractères est " + f1.getCharCount());
		System.out.println("Le nombre de mots est :" + f1.getWordCount());
		System.out.println("Le nombre de phrase est :" + f1.getSentanceCount());
		System.out.println("Le nombre de paragraphes est :" +f1.getParagrapheCount());
		
		System.out.println("---Le 2eme fichier :" + F2.getName()+ " -----");
		
		System.out.println("Le nombre de caractères est " + f2.getCharCount());
		System.out.println("Le nombre de mots est :" + f2.getWordCount());
		System.out.println("Le nombre de phrase est :" + f2.getSentanceCount());
		System.out.println("Le nombre de paragraphes est :" +f2.getParagrapheCount());

	}

}
