package application;
	
import java.io.IOException;


import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;


	
public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) throws IOException {

        
		BorderPane root = new BorderPane();
		root.setLayoutX(23.0);
		root.setLayoutY(7.0);
		root.setPrefHeight(381.0);
		root.setPrefWidth(577.0);
		Pane pane=new Pane();
		Pane pane1 = new Pane();
		pane1.setPrefHeight(381.0);
		pane1.setPrefWidth(263.0);
		root.setRight(pane1);
		ListView<String> liste = new ListView<String>();
		liste.setLayoutX(-5.0);
		liste.setPrefHeight(380.0);
		liste.setPrefWidth(268.0);
		//-------------
		Label label = new Label();
		label.setPrefHeight(379.0);
		label.setPrefWidth(265.0);
		//--------------
		ScrollBar scrollbar = new ScrollBar();
		scrollbar.setLayoutX(249.0);
		scrollbar.setLayoutY(3.0);
		scrollbar.setOrientation(Orientation.VERTICAL);
		scrollbar.setPrefHeight(376.0);
		scrollbar.setPrefWidth(14.0);
		pane1.getChildren().addAll(liste,label,scrollbar);
		
		
		Pane pane2 = new Pane();
		root.setCenter(pane2);
		pane1.setPrefHeight(381.0);
		pane1.setPrefWidth(263.0);
		
		ListView<String> liste2 = new ListView<String>();
		liste2.setLayoutX(-5.0);
		liste2.setPrefHeight(380.0);
		liste2.setPrefWidth(268.0);
		Label label2 = new Label();
		label2.setPrefHeight(379.0);
		label2.setPrefWidth(265.0);
		
		pane2.getChildren().addAll(liste2,label2);
		
		TextField textfield1 = new TextField();
		textfield1.setLayoutX(19.0);
		textfield1.setLayoutY(399.0);
		textfield1.setPrefHeight(25.0);
		textfield1.setPrefWidth(215.0);
		//------------
		TextField textfield2 = new TextField();
		textfield2.setLayoutX(331.0);
		textfield2.setLayoutY(399.0);
		textfield2.setPrefHeight(25.0);
		textfield2.setPrefWidth(204.0);
		//
		Button btn1 = new Button();
		btn1.setLayoutX(241.0);
		btn1.mnemonicParsingProperty().setValue(false);
		btn1.setLayoutY(399.0);
		btn1.setPrefHeight(25.0);
		btn1.setPrefWidth(44.0);
		btn1.setText("Send");
		
		Button btn2 = new Button();
		btn2.setLayoutX(543.0);
		btn2.mnemonicParsingProperty().setValue(false);
		btn2.setLayoutY(399.0);
		btn2.setPrefHeight(25.0);
		btn2.setPrefWidth(57.0);
		btn2.setText("Add");
		
		
		pane.getChildren().addAll(root,textfield1,btn1,textfield2,btn2);
        TitledPane titledpane = new TitledPane();
        titledpane.setPrefWidth(623.0);
        titledpane.setPrefHeight(458.0);
 
        titledpane.setContent(pane);
		//root.getChildren().add(titledpane);
       // root.getChildren().add(pane);
        //root.getChildren().add(pane2);
        Scene scene = new Scene(titledpane);
       
        primaryStage.setTitle("Chat Room");
        primaryStage.setScene(scene);
        primaryStage.show();
	}
			
	public static void main(String[] args) {
		launch(args);
	}
	
}
