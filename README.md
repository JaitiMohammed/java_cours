# Java_Cours

Cours Java Intermediaire - Ingénierie de systèmes d'infromatiqus et logiciel - AU : 2019/2020

## PLAN de cours

*  Les collections
*  Les classes génériques
*  Les flux d'entrée sorite
*  des classes anonymes - fonctions lambda - interfaces fonctionnelles
*  Java Networking
*  Java Threads
*  JavaFX