package metier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Date;


public class Application_Serialisation {

	public static void main(String[] args) {
		
		try {
			File f = new File("banque.txt");
			FileOutputStream fos=new FileOutputStream(f);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			
			
			Operation op1=new Operation(1,new Date(), "CC1", "V", 40000);
			Operation op2=new Operation(2,new Date(), "CC1", "R", 6000);
			
			
			
			oos.writeObject(op1);
			oos.writeObject(op2);
			oos.close();
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
	}

}
