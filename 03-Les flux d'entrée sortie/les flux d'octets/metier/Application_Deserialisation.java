package metier;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;


public class Application_Deserialisation {

	public static void main(String[] args) throws Exception {

		File f=new File("banque.txt");
		FileInputStream fis=new FileInputStream(f);
		ObjectInputStream ois=new ObjectInputStream(fis);
		
		Operation op1=(Operation) ois.readObject();
		//Operation op2=(Operation) ois.readObject();
		
		System.out.println("Num:"+op1.getNumeroOperation());
		System.out.println("Date:"+op1.getDateOperation());
		System.out.println("Compte:"+op1.getNumeroCompte());
		System.out.println("Type:"+op1.getTypeOperation());
		System.out.println("Montant:"+op1.getMontant());

	}

}
