import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileInput_Output {

	public static void main(String[] args) {
		
		try {
			File f1 = new File("img1.jpg");
			FileInputStream fis = new FileInputStream(f1);
			File f2 = new File("img2.jpg");
			FileOutputStream fos = new FileOutputStream(f2);
			
			int c;
			while((c=fis.read())!=-1) {
				fos.write(c);
			}
			fis.close();
			fos.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		

	}

}
