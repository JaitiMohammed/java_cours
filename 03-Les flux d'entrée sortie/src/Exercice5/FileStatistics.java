package Exercice5;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileStatistics{
	
	private String filename;
	
	
	
	public FileStatistics(String filename) {
		this.filename=filename;	
	}
	
	
	
	// Initialisation 
	int countWord = 0;
	int sentenceCount = 0;
	int paragraphCount = 1;
	int characterCount = 0;
	String line;
	
	
	
	
	// nombre de caractères 
	public int getCharCount() throws IOException {
		FileInputStream file = new FileInputStream(filename);
		InputStreamReader input = new InputStreamReader(file); 
        BufferedReader br = new BufferedReader(input); 
        
        while((line = br.readLine()) != null) 
        { 
           
            if(!(line.equals(""))) 
            { 
                  
                characterCount += line.length(); 
               
            } 
        } 
        br.close();
		return characterCount;
	}
	
	// nombre de mots
	
	public int getWordCount() throws IOException {
		FileInputStream file = new FileInputStream(filename);
		InputStreamReader input = new InputStreamReader(file); 
        BufferedReader br = new BufferedReader(input); 
        
        while((line = br.readLine()) != null) 
        { 
        	if(!(line.equals(""))) 
            { 
                  
                
                String[] wordList = line.split("\\s+"); 
                  
                countWord += wordList.length; 
             
  
            } 
        } 
        br.close();
		return countWord;
	}
	
	// nombre de phrase
	public int getSentanceCount() throws IOException {
		
		FileInputStream file = new FileInputStream(filename);
		InputStreamReader input = new InputStreamReader(file); 
        BufferedReader br = new BufferedReader(input); 
        
        while((line = br.readLine()) != null) 
        { 
        	if(!(line.equals(""))) 
            { 
                  
                 
                String[] phraseListe = line.split("[!?.:]+"); 
                  
                sentenceCount += phraseListe.length; 
            }
        } 
        br.close();
		return sentenceCount;
	}
	
	// nombre de paragraphes
	public int getParagrapheCount() throws IOException {
		FileInputStream file = new FileInputStream(filename);
		InputStreamReader input = new InputStreamReader(file); 
        BufferedReader br = new BufferedReader(input); 
        
        while((line = br.readLine()) != null) 
        { 
        	if(line.equals("")) 
            { 
                paragraphCount++; 
            } 
        } 
        br.close();
		return paragraphCount;
	}
	
}
