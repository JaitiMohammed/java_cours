package Exercice5;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {

		FileStatistics f = new FileStatistics("FileStatistics.txt");
		FileStatistics f2 = new FileStatistics("FileStatistics02.txt");
		
		System.out.println("---Le 1er fichier est -----");
		
		System.out.println("Le nombre de caractères est " + f.getCharCount());
		System.out.println("Le nombre de mots est :" + f.getWordCount());
		System.out.println("Le nombre de phrase est :" + f.getSentanceCount());
		System.out.println("Le nombre de paragraphes est :" +f.getParagrapheCount());
		
		System.out.println("---Le 2eme fichier est -----");
		
		System.out.println("Le nombre de caractères est " + f2.getCharCount());
		System.out.println("Le nombre de mots est :" + f2.getWordCount());
		System.out.println("Le nombre de phrase est :" + f2.getSentanceCount());
		System.out.println("Le nombre de paragraphes est :" +f2.getParagrapheCount());

	}

}
