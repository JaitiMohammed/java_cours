package Java_IO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Main1 {

	public static void main(String[] args) throws Exception {
		File fichier = new File("test.txt");
		
		System.out.println("Chemin absolu de fichier =" + fichier.getAbsolutePath());
		System.out.println("Nom de fichier = " + fichier.getName());
		System.out.println("Est-ce qu'il existe =" +fichier.exists());
		System.out.println("Est-ce un reprtoire =" +fichier.isDirectory());
		System.out.println("Est- ce un fichier =" +fichier.isFile());
		
		
		FileReader fr = new FileReader(fichier);
		BufferedReader br = new BufferedReader(fr);
		String s ;
		
		System.out.println("----Affichage de contenu ---");
		while((s=br.readLine())!=null) {
			System.out.println(s);
			int tab = s.length();
			System.out.println(tab);
		}
		br.close();
	}

}
