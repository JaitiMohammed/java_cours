import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReader_Writer {

	public static void main(String[] args) {
		
		try {
			File f1 = new File("notes.txt");
			FileReader fr = new FileReader(f1);
			File f2 = new File("copiesNotes.txt");
			FileWriter fw = new FileWriter(f2);
			
			int c;
			
			while((c=fr.read())!=-1) {
				fw.write(c);
			}
			fr.close();
			fw.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
