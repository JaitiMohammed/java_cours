import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class App {

	public static void main(String[] args) {
		
		try {
			File f = new File("notes.txt");
			
			FileReader fr = new FileReader(f);
			
			BufferedReader br = new BufferedReader(fr);
			
			String s;
			double somme=0;
			int nb=0;
			while((s=br.readLine())!=null) {
				++nb;
				String[] tab = s.split(";");
				double note = Double.parseDouble(tab[2]);
				somme=somme+note;
			}
			
			System.out.println("la moyenne est :" +somme/nb);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
