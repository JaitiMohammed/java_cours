package Exemple_Pratique;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
public class Main1 {

   public static void main(String[] args) {
      List<CD> list = new ArrayList<CD>();
      list.add(new CD("Les arcandiers", "7�", 7d));
      list.add(new CD("Frank Zappa", "Tinseltown rebellion", 10.25d));
      list.add(new CD("Frank Zappa", "Bongo Fury", 10.25d));
      list.add(new CD("King Crimson", "red", 15.30d));
      list.add(new CD("Joe Zawinul", "World tour", 12.15d));
      
      
      System.out.println("Avant le tri : ");
      Iterator<CD> it = list.iterator();
      while(it.hasNext())
         System.out.println(it.next());
      
Collections.sort(list);
      
      System.out.println("Apr�s le tri : ");
      it = list.iterator();
      while(it.hasNext())
         System.out.println(it.next());
      
      System.out.println("Apr�s le tri avec notre comparateur");
      //nous cr�ons une classe anonyme ici, mais rien ne vous emp�che d'en cr�er une dans un fichier s�par�
      Collections.sort(list, new Comparator<CD>(){
         public int compare(CD cd1, CD cd2) {
            Double prix1 = (Double)cd1.getPrix();
            Double prix2 = (Double)cd2.getPrix();
            int result = prix1.compareTo(prix2);
            //dans le cas ou 2 CD auraient le m�me prix...
            if(result == 0){
               return cd1.compareTo(cd2);
            }
            return result;
         }
      });

      it = list.iterator();
      while(it.hasNext())
         System.out.println(it.next());
         
   }
}