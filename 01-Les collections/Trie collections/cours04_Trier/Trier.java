package cours04_Trier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Trier {
	public static void main(String[] args) {
		Set<String> tree = new TreeSet<String>(); // TreeMap +..
		tree.add("Andr�");
		tree.add("Gislain"); 
		tree.add("Matthieu"); 
		tree.add("Cyrille");
		tree.add("Zo�");
		tree.add("Thierry");

		Iterator<String> it = tree.iterator();
		while(it.hasNext())
		   System.out.println(it.next());
		
		//-----------
		System.out.println("-----------------");
		List<Double> list = new ArrayList<Double>();
		list.add(-0.25d);
		list.add(12.52d);
		list.add(56.25d);
		list.add(-45.12d);
		list.add(-100.11d);
		list.add(0.005d);
		Collections.sort(list); // on utilisant sort()
		Iterator<Double> it1 = list.iterator();
		while(it1.hasNext())
		   System.out.println(it1.next());
	}
}
