import java.util.*;
public class _LinkedList {

	public static void main(String[] args) {
		
		
		LinkedList<String> liste_chainee = new LinkedList<String>();
		
		liste_chainee.add("Ahmed");
		liste_chainee.add("Souad");
		liste_chainee.add("Ali");
		liste_chainee.add("Fouad");
		
		
		// Trie
		
		Collections.sort(liste_chainee);
		
		Iterator<String> iterator = liste_chainee.iterator();
		int i=0;
		while(iterator.hasNext()) {
			i++;
			System.out.println("-" + i + " " + iterator.next());
		}
		
		
		
		
	}
}

