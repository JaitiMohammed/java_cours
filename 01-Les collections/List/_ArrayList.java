import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class _ArrayList {
	
	

	/*
	 * 
	 * 
	 * les interfaces de type List sont :
	 * 	- Vector<E>
	 * 	- Stack<E>
	 * 	- ArrayList<E>
	 * 	- LinkedList<E>
	 */
	private static void afficherListe(String nom, final List<String> sousListe) {
		
		int i=0;
		for(String element : sousListe) {
			System.out.println("-"+nom + " - index : " + i +" - valeur :" + element);
			i++;
		}
		
	}
	
	public static void main(String[] args) {
		
		
		List<String> liste = new ArrayList<String>();
		
		liste.add("1");
		liste.add("2");
		liste.add("3");
		liste.add("4");
		liste.add("5");
		
		List<String> sousListe = liste.subList(0, 5);
		afficherListe("sous Liste 01",sousListe);
		
		System.out.println("----------------------");
		

		String index = sousListe.remove(1);
		afficherListe("sous Liste 02",sousListe);
		
		System.out.println("----------------------");
		
		String index2 = sousListe.remove(2);
		afficherListe("sous Liste 03",sousListe);
		
		System.out.println("----------------------");
		System.out.println("Les valeur supprimer sont " + index + " - "
				+index2);
		
		
		
		
		
		
	}

	

}
