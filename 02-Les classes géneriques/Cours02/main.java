package Cours02;

public class main {

	public static void main(String[] args) {
		
		Account<Character> myAccount = new Account<Character>("Khalid",156.00,'$');
		myAccount.showBalance();
		Account<String> otherAccount = new Account<String>("Salim",200.00,"Euros");
		otherAccount.showBalance();
		
		Bank bank = new Bank("BMCE - BANK");
		bank.transfert(myAccount, otherAccount , 1200);
		
		myAccount.showBalance();
		otherAccount.showBalance();
		
		bank.transfert(otherAccount, myAccount, 25);
		
		myAccount.showBalance();
		otherAccount.showBalance();
		
		

	}

}
