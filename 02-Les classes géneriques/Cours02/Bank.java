package Cours02;

public class Bank {
	private String name;
	
	public Bank(String name) {
		this.name=name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public <T, S> void transfert(Account<T> sourceAccount, Account<S> targetAccount,int amount) {
		
		if(sourceAccount.getAmount() >= amount) {
			sourceAccount.removeMoney(amount);
			targetAccount.addMoney(amount);
			
			System.out.println(" ---- Echange bien pass� ----- ");
			System.out.println(sourceAccount.getOwener() + 
					" A envoy� " +amount + sourceAccount.getCurrency()
					+ " � " + targetAccount.getOwener());
		}else {
			
			System.err.println("Transaction impossible");
		}
		
		
	}

	
	
}
