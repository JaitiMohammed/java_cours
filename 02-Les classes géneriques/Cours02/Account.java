package Cours02;

public class Account <T> {
	private String owener;
	private double amount;
	private T currency;
	
	
	public Account(String owener, double amount, T currency) {
		super();
		this.owener = owener;
		this.amount = amount;
		this.currency = currency;
	}
	
	public String getOwener() {
		return owener;
	}
	public void setOwener(String owener) {
		this.owener = owener;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public T getCurrency() {
		return currency;
	}
	public void setCurrency(T currency) {
		this.currency = currency;
	}
	public void addMoney(int amount) {
		this.amount+=amount;
	}
	public void removeMoney(int amount) {
		this.amount-=amount;
	}
	public void showBalance() {
		System.out.println("vous avez actuellement " + amount + " " + currency + " sur votre solde");
	}
	
	
}
