package Cours01;

public class Moteur<T> {
	private T type;
	private String propritaire;
	public Moteur(T type, String propritaire) {
		super();
		this.type = type;
		this.propritaire = propritaire;
	}
	public T getType() {
		return type;
	}
	public void setType(T type) {
		this.type = type;
	}
	public String getPropritaire() {
		return propritaire;
	}
	public void setPropritaire(String propritaire) {
		this.propritaire = propritaire;
	}
	@Override
	public String toString() {
		return "Moteur [type=" + type + ", propritaire=" + propritaire + "]";
	}
	
	
}
