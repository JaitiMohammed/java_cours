package Cours01;

public class Voiture <M,S> {
	
	private M moteur ;
	private String matricule;
	private S prix;
	
	public Voiture(M moteur, String matricule, S prix) {
		super();
		this.moteur = moteur;
		this.matricule = matricule;
		this.prix = prix;
	}
	public M getMoteur() {
		return moteur;
	}
	public void setMoteur(M moteur) {
		this.moteur = moteur;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
	public S getPrix() {
		return prix;
	}
	public void setPrix(S prix) {
		this.prix = prix;
	}
	@Override
	public String toString() {
		return "Voiture [moteur=" + moteur + ", matricule=" + matricule + ", prix=" + prix + "]";
	}
	
	
	

}
