package Sockets;

//A Java program for a Server 
import java.net.*;
import java.util.Scanner;
import java.io.*; 

public class Server 
{ 
	private static final int SERVER_PORT = 8050;

	public Server() throws IOException {
		ServerSocket server = new ServerSocket(SERVER_PORT);
		Socket socket = server.accept();
		InputStream in = socket.getInputStream();
		
		/*
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		
		String line ;
		 while((line = br.readLine())!=null) {
			 System.out.println("Welcom to Server Side");
			 System.out.print(line);
		 }
		in.close();
		br.close();
		socket.close();
		*/
		Scanner scan = new Scanner(in);
		
		while(scan.hasNextLine()) {
			System.out.println(scan.nextLine());
		}
	}
	public static void main(String args[]) throws IOException 
	{ 
		new Server(); 
		
	} 
} 
