package Sockets;

//A Java program for a Client 
import java.net.*; 
import java.io.*; 

public class Client 
{ 
	
	InputStream in ;
	Socket socket ;
	public Client(String Address , int port) {
		
		try {
			socket = new Socket(Address,port);
			PrintWriter pr = new PrintWriter(socket.getOutputStream());
			pr.print("Hello World !");
			pr.flush();
			pr.close();
			socket.close();
		} catch (UnknownHostException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new Client("127.0.0.1",8050);
	}
} 
