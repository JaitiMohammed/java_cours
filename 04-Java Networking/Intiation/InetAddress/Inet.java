package InetAddress;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Inet {

	public static void main(String[] args) throws UnknownHostException {
	
		Scanner Scan = new Scanner(System.in);
		System.out.println("Entrer your adresse");
		String host = Scan.nextLine();
		InetAddress Adresse = InetAddress.getByName(host);
		
		
		System.out.println("Adresse : " + Adresse.getHostAddress());
		System.out.println("Nom : " + Adresse.getHostName());
		System.out.println("Canonique adresse : " + Adresse.getCanonicalHostName());
		Scan.close();
	}

}
