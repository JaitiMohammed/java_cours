package chat;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServeurThreadChat extends Thread {
	private int nbClient;
	private List<Socket> sockets = new ArrayList<Socket>();
	public static void main(String[] args) {
		new ServeurThreadChat().start();
	}
	
	@Override 
	
	public void run() {
		try {
			
			ServerSocket ss = new ServerSocket(1234);
			System.out.println("Demarrage du serveur Mutlithread");
			while(true) {
				Socket s = ss.accept(); // pour chaque connextion il faut creer un thread
				sockets.add(s);
				++nbClient;
				new Conversation(s,nbClient).start();
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void broadcastMessage(String message,Socket source) {
		for(Socket s:sockets) {
			try {
				if(s!=source) {
						PrintWriter pw =new PrintWriter(s.getOutputStream(),true);
						pw.println(message);
				}
			
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			
		}
	}
	
	class Conversation extends Thread{
		private Socket socket;
		private int numero;
		public Conversation(Socket s,int num) {
			this.socket=s;
			this.numero=num;
		}
		@Override
		public void run(){
			try {
				InputStream is = socket.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				
				OutputStream os = socket.getOutputStream();
				PrintWriter pw = new PrintWriter(os,true);
				
				String ip = socket.getRemoteSocketAddress().toString(); // l'adresse IP de client
				System.out.println("Connexion du client numero  " + numero + " IP :" + ip);
				pw.print("Bienvenue vous etes le client numero : " +numero);
				
				while(true) {
					String req=br.readLine();
					broadcastMessage(ip + "a envoy� le message => " + req,socket);
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}

