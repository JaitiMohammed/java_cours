package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientSocket extends Thread{
	
	private PrintWriter printWriter;
	private BufferedReader bufferReader;
	
	public ClientSocket() {
		try {
			Socket s = new Socket("localhost",1234);
			bufferReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
			printWriter = new PrintWriter(s.getOutputStream(),true);
			this.start();
			Scanner clavier = new Scanner(System.in);
			while(true) {
				System.out.println("Donner votre reuqete");
				String req = clavier.nextLine();
				printWriter.println(req);
			}
			
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new ClientSocket();
	}
	
	@Override
		public void run() {
			try {
				String rep ;
				while((rep=bufferReader.readLine())!=null) {
					System.out.println(rep);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}
