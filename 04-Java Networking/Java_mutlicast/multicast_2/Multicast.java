package multicast_2;

import java.net.InetAddress;

class Multicast {
	   public static void main(String[] arg) throws Exception { 
			String nom = arg[0];
			InetAddress groupeIP = InetAddress.getByName("239.255.80.84");
			int port = 8084; 
			new Emetteur(groupeIP, port, nom);
			new Recepteur(groupeIP, port, nom);
	   }
}