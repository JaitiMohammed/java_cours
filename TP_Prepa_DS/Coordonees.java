package TP_ISIL;

public class Coordonees {
	
	private String Tel; 
	private String Adr;
	
	public Coordonees() {
		super();
	}
	public Coordonees(String tel, String adr) {
		super();
		Tel = tel;
		Adr = adr;
	}
	
	public String getTel() {
		return Tel;
	}

	public void setTel(String tel) {
		Tel = tel;
	}

	public String getAdr() {
		return Adr;
	}

	public void setAdr(String adr) {
		Adr = adr;
	}
	
	
	
	

}
