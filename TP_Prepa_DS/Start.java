package TP_ISIL;

public class Start {

	public static void main(String[] args) {
		
		Coordonees cord1 = new Coordonees("0612120202","RUE RIAD AHMED ALI FES");
		Coordonees cord2 = new Coordonees("0602120226","RUE ANWAL CAABLANCA");
		Coordonees cord3 = new Coordonees("0612540202","RUE ALFATH QUARTIER ROMA CASABLANCA");


		Annuaire annuaire = new Annuaire();
		
		annuaire.Ajouter("Ahmed", cord1);
		annuaire.Ajouter("Salim", cord2);
		annuaire.Ajouter("Said", cord3);
		annuaire.affichCoord("Ahmed");
		annuaire.affichCoord("Salim");
		annuaire.affichCoord("Said");
		//annuaire.Suppression("Ahmed");
		annuaire.ListerNoms();
		annuaire.ListerTel();
		annuaire.ListerAdr();
		annuaire.AfficherAnnuaire();

	}

}
