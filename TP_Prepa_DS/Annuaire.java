package TP_ISIL;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/*
 * author : jaiti mohammed
 */

public class Annuaire {
	
	Map<String , Coordonees> annuaire = new HashMap<String,Coordonees>();
	Set<String> keyAnnuaire = annuaire.keySet();
		
	Set<Entry<String , Coordonees>> EntryAnnuaire = annuaire.entrySet();
	Entry<String , Coordonees> Entry;

	
	
	public void Ajouter(String N, Coordonees C) {
		annuaire.put(N, C);
		System.out.println("bien ajout�");
	}
	
	public void affichCoord(String N){
		
		if(annuaire.containsKey(N)) {
			System.out.println("- les coordon�es de : " + N + " est : "+ "Tel :" + annuaire.get(N).getTel() + " / adresse :" + annuaire.get(N).getAdr());
		}
	}
	
	public void ModifCoord(String N,String adr, String tel) {
		if(annuaire.containsKey(N)) {
			annuaire.get(N).setAdr(adr);
			annuaire.get(N).setTel(tel);
		}
		System.out.println("bien modifi�");
	}
	
	public void Suppression(String N) {
		if(annuaire.containsKey(N)) {
			annuaire.remove(N);
		}
		System.out.println("bien supprim�");
	}
	
	
	public void ListerNoms() {
		System.out.println("- Les listes des noms est :");
		if(annuaire.isEmpty()) {
			System.out.println(" ** La liste est vide ** ");
		}
		for(String elmt:keyAnnuaire) {
			System.out.println("* " + elmt);
		}
		
		
	}
	
	//Iterator<Entry<String, Coordonees>> it = EntryAnnuaire.iterator();
	Iterator<String> iter = keyAnnuaire.iterator();
	
	
	Collection<Coordonees> ValueAnnuaire=annuaire.values();

	//Iterator<Coordonees> iterCoord = ValueAnnuaire.iterator();
	
	public void ListerTel() {
		System.out.println("- Les listes des Tel est :");
		if(ValueAnnuaire.isEmpty()) {
			System.out.println(" ** La liste est vide ** ");
		}else {
			for(Coordonees key:ValueAnnuaire) {
				System.out.println(key.getTel());
			}
		}
		
		
	}
	public void ListerAdr() {
		System.out.println("- Les listes des Adresses est :");
		if(ValueAnnuaire.isEmpty()) {
			System.out.println(" ** La liste est vide ** ");
		}else {
			for(Coordonees key:ValueAnnuaire) {
				System.out.println(key.getAdr());
			}
		}
	}


 
	public void AfficherAnnuaire() {
		System.out.println("L'annuaire en totalit�");
		if(annuaire.isEmpty()) {
			System.out.println("** L'annuaire est vide **");
		}
		for(java.util.Map.Entry<String, Coordonees> element:EntryAnnuaire) {
			System.out.println("+++++++++++++++++");
			System.out.println(element.getKey());
			System.out.println(element.getValue().getAdr());
			System.out.println(element.getValue().getTel());
		}
		
	}
	

}
